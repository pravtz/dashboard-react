# Project Front-end Dashboard with React

This is a front end project with react fully responsive in order to serve a layout of a private page of users.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
