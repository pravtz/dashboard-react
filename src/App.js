import React from "react";
import GlobalStyles from "./style/global";
import Aside from "./components/Aside";
import Header from "./components/Header";
import styled from "styled-components";
import Content from "./components/Content";

const App = () => {
  return (
    <>
      <Header />
      <WrapperMain>
        <Aside />
        <Content />
      </WrapperMain>

      <GlobalStyles />
    </>
  );
};
export default App;

const WrapperMain = styled.div`
  display: flex;
`;
