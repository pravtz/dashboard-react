import React, { useState } from "react";
import * as S from "./style";
import { Img } from "react-image";

import {
  MdHome,
  MdStorage,
  MdDescription,
  MdSettings,
  MdShoppingCart,
  MdMenu,
  MdClear,
} from "react-icons/md";

const Aside = () => {
  const [extendedSide, setExtendedSide] = useState(false);
  const ImageProfile = () => <Img src="./image/adele.jpg" />;

  const handlerResponsive = () => {
    return setExtendedSide(!extendedSide);
  };
  return (
    <>
      <S.Wrapper responsive={extendedSide}>
        <S.BottonResponsive>
          <button onClick={() => handlerResponsive()}>
            {" "}
            {extendedSide ? (
              <MdClear className="icon" />
            ) : (
              <MdMenu className="icon" />
            )}{" "}
          </button>
        </S.BottonResponsive>
        <S.PhotoProfile>
          <ImageProfile />
        </S.PhotoProfile>
        <S.NameProfile>Adele</S.NameProfile>
        <S.Menu>
          <S.List>
            <S.Item>
              <S.Link>
                <S.ButtomMenu>
                  <MdHome />
                  <S.ButtomName responsive={extendedSide}>Home</S.ButtomName>
                </S.ButtomMenu>
              </S.Link>
            </S.Item>

            <S.Item>
              <S.Link>
                <S.ButtomMenu>
                  <MdStorage />
                  <S.ButtomName responsive={extendedSide}>
                    My Cadastro
                  </S.ButtomName>
                </S.ButtomMenu>
              </S.Link>
            </S.Item>

            <S.Item>
              <S.Link>
                <S.ButtomMenu>
                  <MdDescription />
                  <S.ButtomName responsive={extendedSide}>
                    Bibliografia
                  </S.ButtomName>
                </S.ButtomMenu>
              </S.Link>
            </S.Item>

            <S.Item>
              <S.Link>
                <S.ButtomMenu>
                  <MdSettings />
                  <S.ButtomName responsive={extendedSide}>
                    Configurações
                  </S.ButtomName>
                </S.ButtomMenu>
              </S.Link>
            </S.Item>

            <S.Item>
              <S.Link>
                <S.ButtomMenu>
                  <MdShoppingCart />
                  <S.ButtomName responsive={extendedSide}>
                    Meus Pedidos
                  </S.ButtomName>
                </S.ButtomMenu>
              </S.Link>
            </S.Item>
          </S.List>
        </S.Menu>
      </S.Wrapper>
    </>
  );
};

export default Aside;
