import styled, { keyframes } from "styled-components";

export const Wrapper = styled.div`
  background-color: var(--bg-second);
  transition: width 0.1s ease-in 0.02s;
  width: ${(props) =>
    props.responsive ? "300px" : "80px"}; //300 | 80 px para responsive
  height: 90vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`;
export const BottonResponsive = styled.div`
  align-self: flex-end;
  button {
    background-color: var(--bg-second);
    border: none;
    color: white;
    outline: none;
  }

  .icon {
    font-size: 1.8em;
  }
`;

export const PhotoProfile = styled.div`
  overflow: hidden;
  width: 41px; //120
  height: 41px; //120
  border-radius: 50%;

  img {
    height: 48px; // 120
    margin: 0 0 0 -16px; // 24
  }
`;
export const NameProfile = styled.h2`
  font-family: "Quicksand", sans-serif;
  font-size: 1.5em;
`;

export const Menu = styled.nav`
  width: 100%;
`;
export const List = styled.ul`
  margin-top: 30px;
  width: 100%;
`;
export const Item = styled.li``;
export const Link = styled.a``;

export const ButtomMenu = styled.div`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  padding: 25px 20px;
  font-size: 1.5rem;
  background-color: var(--bg-second);
  svg {
    width: 1.5em;
    height: 1.5em;
  }

  :hover {
    transition: background-color 0.3s ease-in 0.05s;
    background-color: var(--color-median);
  }
`;
const opacidade = keyframes`
  from {opacity: 0;}
  to {opacity: .6;}
`;
export const ButtomName = styled.span`
  animation: ${opacidade} 0.5s ease-in;

  margin-left: 10px;
  opacity: 0.6;
  display: ${(props) => (props.responsive ? "block" : "none")};
`;
