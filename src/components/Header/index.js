import React from "react";
import * as S from "./style";

// import { Container } from './styles';

function Header() {
  return (
    <S.Wrapper>
      <S.Brand>
        <h1>Hello my Friends</h1>
      </S.Brand>
      <S.Login>
        <S.LoginWrapper>Login</S.LoginWrapper>
      </S.Login>
    </S.Wrapper>
  );
}

export default Header;
