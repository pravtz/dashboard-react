import styled from "styled-components";

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;

  background-color: var(--bg-second);
  width: 100vw;
  height: max(10vh, 50px);
`;
export const Brand = styled.div`
  h1 {
    font-family: "Pacifico", cursive;
    font-size: 2em;
  }
`;
export const Login = styled.div``;
export const LoginWrapper = styled.div``;
